This project is a client for creating an executable environment for GlycanFormatConverter.
Please see also GlycanFormatConverter (https://github.com/glycoinfo/GlycanFormatConverter)

Contact: yamadaissaku@gmail.com

## Release note

* 2024/05/09: released ver 2.10.3.1
   * Updated GlycanFormatConverter version 2.10.3

* 2024/04/25: released ver 2.10.3
   * Change "Files.readString()" functions

* 2024/04/05: released ver 2.10.2
   * Updated GlycanFormatConverter version 2.10.2
   * Support stdin and file

* 2024/04/05: released ver 2.10.1
   * Updated WURCSFramework version 1.2.16
   * Updated GlycanFormatConverter version 2.10.1

* 2023/09/15: released ver 2.10.0
   * Updated WURCSFramework version 1.2.14
   * Updated MolecularFramework version 1.0.0
   * Updated GlycanFormatConverter version 2.10.0

* 2022/10/12: released ver 2.8.0
   * Changed WURCSFramework version to 1.2.9: https://glycoinfo.gitlab.io/wurcsframework/
   * Changed GlycanFormatConverter version to 2.8.0: https://gitlab.com/glycosmos/gfc

* 2021/04/28: released ver 0.1.0

## Requirement
* Java 8 (or later)
* maven 3.6 (or later)

## Build

```
git clone https://gitlab.com/glycoinfo/GlycanFormatConverter-cli.git
```

```
cd GlycanFormatConverter-cli
```

```
mvn clean compile assembly:single
```

* When compilation in finished, jar file is created in the target folder.
>[INFO] Building jar: /../../GlycanFormatConverter-cli/target/GlycanFormatConverter-cli.jar

## Usage
```
java -jar ./target/GlycanFormatConverter-cli.jar -i <FORMAT> -e <FORMAT> -seq <SEQUENCE>
```

## Options
|Option|Argument|Description|
| ---- |  ----  |    ----   |
|-e, --export|FORMAT=<br>[IUPAC-Short\|IUPAC-Condensed\|IUPAC-Extended\|GlycoCT\|WURCS\|GlycanWeb]|export format|
|-h, --help||Show usage help|
|-i, --import|FORMAT=<br>[IUPAC-Condensed\|IUPAC-Extended\|GlycoCT\|KCF\|LinearCode\|WURCS]|import format|
|-seq, --sequence|SEQUENCE|Glycan text format|

## Supported formats
|Input|IUPAC-Extended|IUPAC-Condensed|IUPAC-Short|GlycoCT|WURCS|GlycanWeb|
|---|:---:|:---:|:---:|:---:|:---:|:---:|
|WURCS|✔︎|✔|✔|✔︎︎| |✔|
|IUPAC-Extended| |✔|✔| |✔|✔|
|IUPAC-Condensed| | |✔| |✔|✔|
|KCF|✔|✔|✔| |✔|✔|
|LinearCode|✔|✔|✔| |✔|✔|
|GlycoCT|✔|✔|✔| |✔|✔|


## Example
1. WURCS to IUPAC-Extended

```
java -jar ./target/GlycanFormatConverter-cli.jar -i WURCS -e IUPAC-Extended -seq 'WURCS=2.0/5,9,8/[a2122h-1x_1-5][a2112h-1b_1-5][a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-2-3-2-4-5-3-2-5/a4-b1_b3-c1_b6-g1_c4-d1_d2-e1_d6-f2_g4-h1_h6-i2'
```

* result

```
α-D-Neup5Ac-(2→6)[α-L-Fucp-(1→2)]-β-D-Galp-(1→4)-β-D-GlcpNAc-(1→3)[α-D-Neup5Ac-(2→6)-β-D-Galp-(1→4)-β-D-GlcpNAc-(1→6)]-β-D-Galp-(1→4)-?-D-Glcp-(1→
```

2. IUPAC-Extended to WURCS

```
java -jar ./target/GlycanFormatConverter-cli.jar -i IUPAC-Extended -e WURCS -seq "α-D-Neup5Ac-(2→6)[α-L-Fucp-(1→2)]-β-D-Galp-(1→4)-β-D-GlcpNAc-(1→3)[α-D-Neup5Ac-(2→6)-β-D-Galp-(1→4)-β-D-GlcpNAc-(1→6)]-β-D-Galp-(1→4)-?-D-Glcp-(1→" | grep WURCS=2.0 | grep -v DEBUG
```

* result

```
WURCS=2.0/5,9,8/[a2122h-1x_1-5][a2112h-1b_1-5][a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-2-3-2-4-5-3-2-5/a4-b1_b3-c1_b6-g1_c4-d1_d2-e1_d6-f2_g4-h1_h6-i2
```


3. WURCS to GlycoCT
```
java -jar ./target/GlycanFormatConverter-cli.jar -i WURCS -e GlycoCT -seq 'WURCS=2.0/5,9,8/[a2122h-1x_1-5][a2112h-1b_1-5][a2122h-1b_1-5_2*NCC/3=O][a1221m-1a_1-5][Aad21122h-2a_2-6_5*NCC/3=O]/1-2-3-2-4-5-3-2-5/a4-b1_b3-c1_b6-g1_c4-d1_d2-e1_d6-f2_g4-h1_h6-i2'
```

* result
```
RES
1b:x-dglc-HEX-1:5
2b:b-dgal-HEX-1:5
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dgal-HEX-1:5
6b:a-lgal-HEX-1:5|6:d
7b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
8s:n-acetyl
9b:b-dglc-HEX-1:5
10s:n-acetyl
11b:b-dgal-HEX-1:5
12b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d
13s:n-acetyl
LIN
1:1o(4+1)2d
2:2o(3+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(2+1)6d
6:5o(6+2)7d
7:7d(5+1)8n
8:2o(6+1)9d
9:9d(2+1)10n
10:9o(4+1)11d
11:11o(6+2)12d
12:12d(5+1)13n
```


4. GlycoCT to WURCS

```
cat ./glycoct.txt | java -jar ./target/GlycanFormatConverter-cli.jar -i glycoct -e wurcs
```

or

```
java -jar ./target/GlycanFormatConverter-cli.jar -i glycoct -e wurcs -seq ./glycoct.txt
```

* result
```
WURCS=2.0/6,16,15/[a2122h-1x_1-?_2*NCC/3=O][a2122h-1x_1-5_2*NCC/3=O][a1122h-1x_1-5][a2112h-1x_1-5][Aad21122h-2x_2-6_5*NCC/3=O][a1221m-1x_1-5]/1-2-3-3-2-2-3-2-2-4-5-4-5-4-5-6/a?-b1_b?-c1_c?-d1_c?-g1_d?-e1_d?-f1_g?-h1_g?-i1_j?-k2_l?-m2_n?-o2_j1-a?|b?|c?|d?|e?|f?|g?|h?|i?}_l1-a?|b?|c?|d?|e?|f?|g?|h?|i?}_n1-a?|b?|c?|d?|e?|f?|g?|h?|i?}_p1-a?|b?|c?|d?|e?|f?|g?|h?|i?}
```


